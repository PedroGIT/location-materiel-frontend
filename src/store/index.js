import Vue from 'vue';
import Vuex from 'vuex';
import Cookies from 'js-cookie';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    selectedItem: null,
    basketProducts: [],
    user: {
      userId: -1,
      admin: false,
      superAdmin: false,
    },
    nbItemBasket: 0,
  },
  mutations: {
    updateSelectedItem(state, item) {
      state.selectedItem = item;
    },
    addProductToBasket(state, product) {
      state.basketProducts.push(product);
      state.nbItemBasket += 1;
    },
    removeProductFromBasket(state, product) {
      state.basketProducts.splice(state.basketProducts.indexOf(product), 1);
      state.nbItemBasket -= 1;
    },
    changeUser(state, payload) {
      state.user.userId = payload.userId;
      state.user.admin = payload.admin;
      state.user.superAdmin = payload.superAdmin;
    },
    decoUser(state) {
      state.user.userId = -1;
      state.user.admin = false;
      state.user.superAdmin = false;
    },
    initNbItemBasket(state) {
      state.nbItemBasket = state.basketProducts.length;
    },
    emptyBasket(state) {
      state.basketProducts = [];
    },
  },
  actions: {

  },
  plugins: [createPersistedState({
    getItem: (key) => Cookies.get(key),
    setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
    removeItem: (key) => Cookies.remove(key),
  })],
});
