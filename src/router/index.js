import Vue from 'vue';
import VueRouter from 'vue-router';
import AdminAdd from '../views/AdminAdd.vue';
import AdminBorrows from '../views/AdminBorrows.vue';
import AdminDemand from '../views/AdminDemand.vue';
import AdminManagement from '../views/AdminManagement.vue';
import AdminHistoric from '../views/AdminHistoric.vue';
import SuperAdminManagement from '../views/SuperAdminManagement.vue';
import Login from '../views/Login.vue';
import UserBasket from '../views/UserBasket.vue';
import UserDemand from '../views/UserDemand.vue';
import UserHistoric from '../views/UserHistoric.vue';
import UserHome from '../views/UserHome.vue';
import UserProduct from '../views/UserProduct.vue';
import UserAddBasket from '../views/UserAddBasket.vue';


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/add',
    name: 'Add',
    component: AdminAdd,
  },
  {
    path: '/Borrows',
    name: 'Borrows',
    component: AdminBorrows,
  },
  {
    path: '/admindemand',
    name: 'AdminDemand',
    component: AdminDemand,
  },
  {
    path: '/adminmanagement',
    name: 'AdminManagement',
    component: AdminManagement,
  },

  {
    path: '/login',
    name: 'Login',
    component: Login,
  },

  {
    path: '/basket',
    name: 'Basket',
    component: UserBasket,
  },
  {
    path: '/userdemand',
    name: 'UserDemand',
    component: UserDemand,
  },
  {
    path: '/historic',
    name: 'Historic',
    component: UserHistoric,
  },
  {
    path: '/home',
    name: 'Home',
    component: UserHome,
  },
  {
    path: '/product',
    name: 'Product',
    component: UserProduct,
  },
  {
    path: '/SuperAdminManagement',
    name: 'SuperAdminManagement',
    component: SuperAdminManagement,
  },
  {
    path: '/UserAddBasket',
    name: 'UserAddBasket',
    component: UserAddBasket,
  },
  {
    path: '/AdminHistoric',
    name: 'AdminHistoric',
    component: AdminHistoric,
  },

];

const router = new VueRouter({
  routes,
});

export default router;
